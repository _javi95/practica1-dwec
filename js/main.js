/*********************************
Autor: Fco Javier Ortega Carrasco
Fecha creación: 22/10/2018
Última modificación: 14/12/2018
Versión: 1.0
***********************************/
$(document).ready(function(){

    $("#myFrame").attr("src","html/presentacion.html");

    $("#p1").click(function(){

        $("#myFrame").attr("src","html/calculadora.html");
    });

    $("#p2").click(function(){

        $("#myFrame").attr("src","html/agenda.html");

    });

});