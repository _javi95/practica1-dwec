/*********************************
Autor: Fco Javier Ortega Carrasco
Fecha creación: 22/10/2018
Última modificación: 14/12/2018
Versión: 1.0
***********************************/

var contactos = [];

const MAX_CONTACTOS = 100;

var personaSelec;

var regexLetras = /^[a-zA-Z\u00f1\u00d1\u00E0-\u00FC\u00C0-\u00DC]{1}([a-zA-Z\u00f1\u00d1\u00E0-\u00FC\u00C0-\u00DC\s]*)$/;
// permitimos sólo letras(Ñ/ñ ,acentos y espacios) y no puede empezar espacio en blanco

var regexTelefono = /^(6|9|7){1}\d{8}$/;

var nuevo = false;

var editar = false;

/**
 * Clase Persona
 * @param {*} nombre Nombre 
 * @param {*} apellidos Apellidos 
 * @param {*} telefono Teléfono 
 * @param {*} fecNac Fecha de nacimiento 
 */
function Persona(nombre, apellidos, telefono, fecNac) {

    this.nombre = nombre;

    this.apellidos = apellidos;

    this.telefono = telefono;

    this.fecNac = fecNac;
}

$("document").ready(function () {

    contactos.push(new Persona("Juan", "Dominguez Pérez", "689784567", "15-06-1999"),
        new Persona("María", "Martín Carrasco", "659445766", "07-02-1995"),
        new Persona("Daniel", "Gómez Romero", "657888923", "25-08-1994"),
        new Persona("Pepe", "Hernandez García", "612575678", "05-02-1998"));

    muestraPersona(contactos[0]);

    personaSelec = contactos[0];

    actualizaPos();

    muestraResumen();

    $("#btnPrimero").click(function () {

        muestraPersona(contactos[0]);

        personaSelec = contactos[0];

        actualizaPos();
    });

    $("#btnAnterior").click(function () {

        muestraPersona(contactos[contactos.indexOf(personaSelec) - 1]);

        personaSelec = contactos[contactos.indexOf(personaSelec) - 1];

        actualizaPos();
    });

    $("#btnSiguiente").click(function () {

        muestraPersona(contactos[contactos.indexOf(personaSelec) + 1]);

        personaSelec = contactos[contactos.indexOf(personaSelec) + 1];

        actualizaPos();
    });

    $("#btnUltimo").click(function () {

        muestraPersona(contactos[contactos.length - 1]);

        personaSelec = contactos[contactos.length - 1];

        actualizaPos();
    });

    $("#btnVer").click(function () {

        if (isNaN($("#txtMostrar").val())

            || (Number)($("#txtMostrar").val()) <= contactos.indexOf(contactos[0]) ||

            (Number)($("#txtMostrar").val()) > contactos.length) {

            muestraError($("#txtMostrar"));

            $("#txtMostrar").focus();

        } else {

            restauraError($("#txtMostrar"));

            muestraPersona(contactos[$("#txtMostrar").val() - 1]);

            personaSelec = contactos[$("#txtMostrar").val() - 1];

            actualizaPos();
        }
    });

    $("#btnNuevo").click(function () {

        if (contactos.length == 100) {

            $("#myModal2").modal(
                { show: true }
            );

        } else {

            nuevo = true;

            limpiaFormulario();

            soloLectura(false);

            $("#nombre").focus();

            cambiaBtns(true); // desactivamos botones búsqueda, edición etc

            cambiaBtnsRegistro(true); // desactivamos botones de posicionamiento
        }
    });

    $("#btnEditar").click(function () {

        editar = true;

        $("#nombre").focus();

        cambiaBtns(true);

        cambiaBtnsRegistro(true);

        soloLectura(false);
    });

    $("#btnGuardar").click(function () {

        if (editar || nuevo)
            validaForm();
    });

    $("#btnBorrar2").click(function () { // botón borrar del modal

        var posActual;

        // guardamos posición de la persona que borramos para mostrar la que ocupe luego su lugar

        if (contactos.indexOf(personaSelec) == contactos.length - 1 && contactos.length != 1) { // borramos el último

            posActual = contactos.indexOf(personaSelec) - 1;

        } else {

            posActual = contactos.indexOf(personaSelec);
        }

        contactos.splice(contactos.indexOf(personaSelec), 1);

        if (contactos.length > 0) {

            personaSelec = contactos[posActual];

            actualizaPos();

            muestraPersona(personaSelec);

        } else {

            personaSelec = null;

            $("#txtPos").text("0 de 0");

            limpiaFormulario();

            cambiaBtns(true);
        }

        muestraResumen();
    });

    $("#btnBuscar").click(function () {

        var campoBuscar = $("#opcion_buscar").val();

        var valor = $("#txtBuscar").val();

        busqueda(campoBuscar, valor);

    })

});

/**
 * Función que busca una persona en el array en función del campo elegido
 * @param {*} campo Campo de una persona a buscar
 * @param {*} valor Valor del campo a buscar
 */
function busqueda(campo, valor) {

    restauraError($("#txtBuscar"));

    var personEncontrada;

    switch (campo) {

        case "nombre":

            personEncontrada = contactos.find(persona => persona.nombre.toLowerCase() == valor.toLowerCase());

            break;

        case "apellidos":

            personEncontrada = contactos.find(persona => persona.apellidos.toLowerCase() == valor.toLowerCase());

            break;

        case "telefono":

            personEncontrada = contactos.find(persona => persona.telefono.toLowerCase() == valor.toLowerCase());

            break;

        case "fecha":

            personEncontrada = contactos.find(persona => persona.fecNac.toLowerCase() == valor.toLowerCase());

            break;
    }

    if (personEncontrada != null) { // tenemos una coincidencia

        muestraPersona(personEncontrada);

        personaSelec = personEncontrada;

        actualizaPos();

    } else { // no hay coincidencias

        $("#txtBuscar").focus();

        muestraError($("#txtBuscar"));
    }
}

/**
 * Función que actualiza el cuadro de registro en función de las operaciones que realicemos
 */
function actualizaPos() {

    $("#txtPos").text((contactos.indexOf(personaSelec) + 1) + " de " + contactos.length);

    if (contactos.length == 0 || contactos.length == 1) { // no podemos desplazarnos

        $("#btnPrimero").prop("disabled", true);

        $("#btnAnterior").prop("disabled", true);

        $("#btnSiguiente").prop("disabled", true);

        $("#btnUltimo").prop("disabled", true);

    } else {

        if (contactos.indexOf(personaSelec) == 0) { // primera persona seleccionada

            $("#btnPrimero").prop("disabled", true);

            $("#btnAnterior").prop("disabled", true);

            $("#btnSiguiente").prop("disabled", false);

            $("#btnUltimo").prop("disabled", false);

        } else if (contactos.indexOf(personaSelec) == contactos.length - 1) { // última persona seleccionada

            $("#btnSiguiente").prop("disabled", true);

            $("#btnUltimo").prop("disabled", true);

            $("#btnPrimero").prop("disabled", false);

            $("#btnAnterior").prop("disabled", false);


        } else { // otra persona seleccionada

            $("#btnPrimero").prop("disabled", false);

            $("#btnAnterior").prop("disabled", false);

            $("#btnSiguiente").prop("disabled", false);

            $("#btnUltimo").prop("disabled", false);
        }
    }
}

/**
 * Función que muestra los datos de una persona en el formulario
 * @param {*} persona Persona a mostrar en el formulario 
 */
function muestraPersona(persona) {

    $("#nombre").val(persona.nombre);

    $("#apellidos").val(persona.apellidos);

    $("#telefono").val(persona.telefono);

    $("#fec_nac").val(persona.fecNac.toString().split("-").reverse().join("-")); // formato requerido por el input yyyy/mm/dd
}

/**
 * Función que obtiene una persona a partir de los datos del formulario
 */
function obtenerPersona() {

    return new Persona($("#nombre").val(), $("#apellidos").val(), $("#telefono").val(),
        $("#fec_nac").val().split("-").reverse().join("-"));
}

/**
 * Función que valida los campos del formulario antes de añadir y editar, mostrando los errores que hubiese
 */
function validaForm() {

    restauraError($("#nombre"));

    $("#ayudaNombre").attr("hidden", "true"); // ocultamos el mensaje de ayuda si se está mostrando

    restauraError($("#apellidos"));

    $("#ayudaApellidos").attr("hidden", "true");

    restauraError($("#telefono"));

    $("#ayudaTelefono").attr("hidden", "true");

    restauraError($("#fec_nac"));

    $("#ayudaFec").attr("hidden", "true");

    if (regexLetras.test($("#nombre").val()) && regexLetras.test($("#apellidos").val())

        && regexTelefono.test($("#telefono").val()) && fechaValida($("#fec_nac").val())) { // no hay errores

        var nuevaPersona = obtenerPersona();

        if (nuevo) { // añadir

            contactos.push(nuevaPersona);

            personaSelec = nuevaPersona;

            nuevo = false;

        } else { // editar

            contactos[contactos.indexOf(personaSelec)] = nuevaPersona;

            personaSelec = nuevaPersona;

            editar = false;
        }

        muestraPersona(personaSelec);

        actualizaPos();

        muestraResumen();

        soloLectura(true);

        cambiaBtns(false);

    } else { // hay errores

        if (!regexLetras.test($("#nombre").val())) {

            muestraError($("#nombre"));

            $('#ayudaNombre').removeAttr('hidden');
        }

        if (!regexLetras.test($("#apellidos").val())) {

            muestraError($("#apellidos"));

            $('#ayudaApellidos').removeAttr('hidden');
        }

        if (!regexTelefono.test($("#telefono").val())) {

            muestraError($("#telefono"));

            $('#ayudaTelefono').removeAttr('hidden');
        }

        if (!fechaValida($("#fec_nac").val())) {

            muestraError($("#fec_nac"));

            $('#ayudaFec').removeAttr('hidden');
        }
    }
}

/**
 * Función que lista los contactos de la agenda
 */
function muestraResumen() {

    $("#tabla").empty();

    $("#txtEntradas").text("( Total entradas almacenadas: " + contactos.length +

        " // Espacio libre: " + (MAX_CONTACTOS - (Number)(contactos.length)) + " )");

    contactos.forEach((element, index) => {

        $("#tabla").append(

            "<tr>" +

            "<td scope='row'>" + (index + 1) + "</td>" +

            "<td>" + element.nombre + "</td>" +

            "<td>" + element.apellidos + "</td>" +

            "<td>" + element.telefono + "</td>" +

            "<td>" + element.fecNac + "</td>" +

            "</tr>"
        )
    });
}

/**
 * Función que vacía los campos del formulario
 */
function limpiaFormulario() {

    $("#nombre").val("");

    $("#apellidos").val("");

    $("#telefono").val("");

    $("#fec_nac").val("");
}

/**
 * Función que cambia el estado algunos botones del formulario
 * @param {*} valor Valor que tomará la propiedad de los botones
 */
function cambiaBtns(valor) {

    $("#btnVer").prop("disabled", valor ? true : false);

    $("#btnNuevo").prop("disabled", valor && contactos.length > 0 ? true : false);

    $("#btnBorrar1").prop("disabled", valor ? true : false);

    $("#btnEditar").prop("disabled", valor ? true : false);

    $("#btnBuscar").prop("disabled", valor ? true : false);
}

/**
 * Función que cambia el estado algunos botones del cuadro de registro
 * @param {*} valor Valor que tomará la propiedad de los botones
 */
function cambiaBtnsRegistro(valor) {

    $("#btnPrimero").prop("disabled", valor ? true : false);

    $("#btnAnterior").prop("disabled", valor ? true : false);

    $("#btnSiguiente").prop("disabled", valor ? true : false);

    $("#btnUltimo").prop("disabled", valor ? true : false);
}

/**
 * Función que determinará si ciertos controles del formulario serán de solo lectura
 * @param {*} valor Valor que indicará si se habilita o no el control
 */
function soloLectura(valor) {

    $("#nombre").prop("readonly", valor ? true : false);

    $("#apellidos").prop("readonly", valor ? true : false);

    $("#telefono").prop("readonly", valor ? true : false);

    $("#fec_nac").prop("readonly", valor ? true : false);
}

/**
 * Función que determina si una fecha es correcta
 * @param {*} fecha Fecha a validar
 */
function fechaValida(fecha) {
    
    var year = new Date(fecha).getFullYear();

    return year + 1 >= 1900 && year <= 2020; // permitimos fechas entre 1900 y 2020
}

/**
 * Función que mostrará error en un campo del formulario
 * @param {*} input Control del formulario donde se muestra el error
 */
function muestraError(input) {
    if (!input.hasClass("is-invalid")) {
        input.addClass("is-invalid");
    }
}

/**
 * Función que quitará el error de un campo del formulario
 * @param {*} input Control del formulario sobre el que realizamos el cambio
 */
function restauraError(input) {
    if (input.hasClass("is-invalid"))
        input.removeClass("is-invalid");
}
