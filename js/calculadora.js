/*********************************
Autor: Fco Javier Ortega Carrasco
Fecha creación: 22/10/2018
Última modificación: 14/12/2018
Versión: 1.0
***********************************/

var pantalla;

var acumulador = 0;

var num2 = 0;

var memoria = 0;

var btnNumeros;

var btnOperadores;

var ultimoChar = "";

var operador = "";

var primerCero = true; // variable para controlar el 0 inicial

var error = false; // variable para posibles errores de cálculo (infinito, dividir entre 0...etc)

var mostrandoResultado = false; // variable para saber si el número que hay en pantalla.value es un resultado

var realizandoOperacion = false; // variable que controla si hemos iniciado una operación

const LONGITUD_MAX = 15;

$("document").ready(function () {

    $(".calculator").animate({ right: '0px' });

    pantalla = document.getElementById("resultado");

    btnNumeros = document.querySelectorAll(".number");

    for (let index = 0; index < btnNumeros.length; index++) {

        btnNumeros[index].addEventListener("click", muestraNumero); // listener para todos los botones numéricos
    }

    btnOperadores = document.querySelectorAll(".operator");

    for (let index = 0; index < btnOperadores.length; index++) {

        btnOperadores[index].addEventListener("click", operacion); // listener para todos los operadores
    }

    // MC, MR, MS, M+, 

    $(".Memory").click(function (e) {

        switch (e.target.innerHTML) {

            case "MC":

                memoria = 0;

                break;

            case "MR":

                if (!error) {
                    redondeaResultado(memoria);

                    if (pantalla.value == 0) {
                        primerCero = true;
                    }

                    if (!realizandoOperacion)

                        acumulador = parseFloat(pantalla.value);

                    else {

                        num2 = parseFloat(pantalla.value);
                    }
                }

                break;

            case "M+":

                if (!error)
                    memoria = parseFloat(memoria) + parseFloat(pantalla.value);

                break;

            case "M-":

                if (!error)
                    memoria = parseFloat(memoria) - parseFloat(pantalla.value);

                break;

            case "MS":

                if (!error)
                    memoria = parseFloat(pantalla.value);

                break;
        }

        mostrandoResultado = true;
    });

    // resto de botones

    $(".igual").click(muestraResultado);

    $(".C").click(clear);

    $(".CE").click(CE);

    $(".coma").click(coma);

    $(".raiz").click(raiz);

    $(".retroceso").click(retroceso);

    $(".signo").click(cambiaSigno);

    $(".unoEntreX").click(unoEntreX);

    $(".porcentaje").click(porcentaje);
});

/**
 * Función que muestra un número por pantalla
 * @param {*} e Número pulsado
 */
function muestraNumero(e) {

    var numero = e.target.innerHTML;

    if (primerCero && numero == 0 && !error) { // 0 inicial y pulsamos el 0

        pantalla.value = "0";

    } else {

        // casos en los que borramos la pantalla antes de concatenar el número
        if (primerCero || mostrandoResultado || error || esOperador(ultimoChar) || realizandoOperacion && pantalla.value == "0") {

            pantalla.value = "";

            primerCero = (mostrandoResultado || error) && numero == 0 ? true : false;

            if (mostrandoResultado) {

                mostrandoResultado = false;

                if (ultimoChar != "=" && realizandoOperacion) { // 1 + √5 -->  1 + (número pulsado)

                    $("#operacion").text(acumulador + " " + operador + " " + pantalla.value);

                } else {

                    $("#operacion").text("");
                }

            }

            if (error)
                restauraError();

        } else { // introduces un número después del 0 inicial

            primerCero = false;
        }

        if (pantalla.value.length < LONGITUD_MAX) {

            pantalla.value += numero;

            ultimoChar = numero;

            if (!realizandoOperacion) { // si se está realizando una operación, no pisamos el valor del acumulador

                acumulador = parseFloat(pantalla.value);

            } else {

                num2 = parseFloat(pantalla.value);
            }
        }
    }
}

/**
 * Función que determina la operación a realizar
 * @param {*} e Operación pulsada
 */
function operacion(e) {

    if (!error) {

        if (realizandoOperacion && !esOperador(ultimoChar) || ultimoChar == "%") { // 1 + 7 * -> 8 *

            calcula();

            operador = e.target.innerHTML;
        }

        else { // no se está realizando operación y pulsamos un operador

            operador = e.target.innerHTML;

            realizandoOperacion = true;

            pantalla.value = pantalla.value.includes(".") && !pantalla.value.includes("e") ? parseFloat(pantalla.value) : pantalla.value; // 3. + --> 3 +

        }

        $("#operacion").text(acumulador + " " + operador);

        ultimoChar = operador;
    }
}

/**
 * Función que calcula el resultado de la operación
 */
function calcula() {

    if (esOperador(ultimoChar)) // 6 + =
        num2 = parseFloat(pantalla.value);

    switch (operador) {

        case "+":

            redondeaResultado(parseFloat(acumulador) + parseFloat(num2)); // mostramos el resultado

            break;

        case "-":

            redondeaResultado(parseFloat(acumulador) - parseFloat(num2));

            break;

        case "*":

            redondeaResultado(parseFloat(acumulador) * parseFloat(num2));

            break;

        case "/":

            if (parseFloat(pantalla.value) == 0) {

                muestraError();

            } else {

                redondeaResultado(parseFloat(acumulador) / parseFloat(num2));
            }

            break;
    }

    acumulador = parseFloat(pantalla.value);

    if (acumulador == 0) {
        primerCero = true;
    }
}

/**
 * Función que calcula el porcentaje
 */
function porcentaje() {

    if (!error && realizandoOperacion) {

        redondeaResultado((parseFloat(acumulador) * parseFloat(pantalla.value)) / 100);

        num2 = parseFloat(pantalla.value);

        $("#operacion").text(acumulador + " " + operador + " " + (Math.round(parseFloat(pantalla.value) * 100) / 100));
        // No se muestra al completo para evitar desbordamientos
        ultimoChar = "%";
    }
}

/**
 * Función que muestra el resultado cuando pulsamos el botón igual
 */
function muestraResultado() {

    if ((realizandoOperacion || ultimoChar == "=") && !error) {

        calcula();

        ultimoChar = "=";

        realizandoOperacion = false;

        mostrandoResultado = true;

        $("#operacion").text("");

    } else if (error) {

        clear();
    }
}

/**
 * Función que añadira la coma a nuestra operación si es posible
 */
function coma() {

    if ((!pantalla.value.includes('.') || mostrandoResultado || realizandoOperacion) && !error) {

        if (mostrandoResultado || esOperador(ultimoChar)) { // mostrando resultado después de operar o coma después de operador

            pantalla.value = "0.";

            if (mostrandoResultado)
                mostrandoResultado = false;

        } else if (!pantalla.value.includes('.')) {

            pantalla.value += ".";
        }

        if (primerCero)
            primerCero = false;

        ultimoChar = '.';
    }
}

/**
 * Función que limpia la pantalla y reinicia la operación
 */
function clear() {

    acumulador = 0;

    num2 = 0;

    pantalla.value = 0;

    primerCero = true;

    ultimoChar = "";

    operador = "";

    mostrandoResultado = false;

    realizandoOperacion = false;

    $("#operacion").text("");

    if (error) {

        restauraError();
    }
}

/**
 * Función que reinicia la última entrada realizada si estamos realizando una operación
 */
function CE() {

    if (realizandoOperacion && !error) {

        pantalla.value = "0";

        num2 = 0;

        primerCero = true;

        $("#operacion").text(acumulador + " " + operador);
    }
}

/**
 * Función para eliminar el último número de la pantalla si es posible
 */
function retroceso() {

    if (!error && !mostrandoResultado && !esOperador(ultimoChar)) {

        if (!primerCero && pantalla.value.length > 1) {

            pantalla.value = pantalla.value.slice(0, pantalla.value.length - 1);

            if (pantalla.value.length == 1 && esOperador(pantalla.value) || pantalla.value == "-0") {
                
                // dejar un signo en la pantalla o volvemos al 0 inicial

                pantalla.value = 0;

                primerCero = true;
            }

        } else { // eliminamos todos los caracteres de la pantalla

            pantalla.value = 0;

            primerCero = true;
        }

        ultimoChar = pantalla.value[pantalla.value.length - 1];

        if (!realizandoOperacion)
            acumulador = parseFloat(pantalla.value);
        else {
            num2 = parseFloat(pantalla.value);
        }

    } else if (error) {

        clear();
    }
}

/**
 * Función que cambia el signo al número mostrado por pantalla si es posible
 */
function cambiaSigno() {

    if (!error && pantalla.value != "0") {

        if (pantalla.value.includes("-")) { // es negativo

            pantalla.value = pantalla.value.slice(1, pantalla.value.length); // le quitamos el caracter '-'

        } else { // es positivo o igual a 0

            if (!primerCero) {

                if (!pantalla.value.includes("-"))
                    pantalla.value = "-" + pantalla.value;
            }
        }

        if (!realizandoOperacion) {

            acumulador = parseFloat(pantalla.value);

        } else {

            num2 = parseFloat(pantalla.value);
        }

        ultimoChar = "+/-";
    }
}

/**
 * Función que calcula la raíz del número en pantalla 
 */
function raiz() {

    if (!error) {

        var resultado = Math.sqrt(parseFloat(pantalla.value));

        if (!isNaN(resultado)) { // raíz válida

            if (resultado == 0) {

                primerCero = true;

            } else {

                if (!realizandoOperacion) {

                    $("#operacion").text("√(" + acumulador + ")");

                    acumulador = resultado;

                } else {

                    $("#operacion").text(acumulador + " " + operador + " " + "√(" + Math.round(parseFloat(pantalla.value) * 100) / 100 + ")");

                    num2 = resultado;
                }
            }

            redondeaResultado(resultado);

            mostrandoResultado = true;
        }

        else { // raíces negativas

            $("#operacion").text("√(" + pantalla.value + ")");

            muestraError();
        }

        ultimoChar = "√";
    }
}

/**
 * Función que divide 1 entre el número que haya en pantalla
 */
function unoEntreX() {

    if (!error) {

        var resultado;

        if (parseFloat(pantalla.value) == 0) {

            $("#operacion").text("1/" + "(" + pantalla.value + ")");

            muestraError();

        } else {

            resultado = (1 / parseFloat(pantalla.value));

            if (!realizandoOperacion) {

                $("#operacion").text("1/" + "(" + acumulador + ")");

                acumulador = resultado;

            } else {

                $("#operacion").text(acumulador + " " + operador + " " + "1/(" + Math.round(parseFloat(pantalla.value) * 100) / 100 + ")");

                num2 = resultado;
            }
        }

        redondeaResultado(resultado);

        mostrandoResultado = true;

        ultimoChar = "1/x"
    }
}

/**
 * Función que determina si un carácter es un operador
 * @param {*} char Carácter a examinar
 */
function esOperador(char) {

    return char == '+' || char == '-' || char == '/' || char == '*' ? true : false;
}

/**
 * Función que muestra un texto de error en pantalla
 */
function muestraError() {

    pantalla.value = "** ERROR **";

    $("#resultado").css({ "text-align": "center", "color": "red", "padding-right": "0px", "padding-left": "6px" });

    error = true;
}

/**
 * Función que quita un texto de error de la pantalla
 */
function restauraError() {

    error = false;

    $("#resultado").css({ "text-align": "right", "color": "", "padding-right": "10px" });

    $("#operacion").text("");
}

/**
 * Función que redondea el resultado antes de mostrarlo en pantalla
 * @param {*} resultado Resultado numérico a formatear
 */
function redondeaResultado(resultado) {

    if (!isFinite(resultado)) { // se ha producido un desbordamiento

        $("#operacion").text("");

        muestraError();

    } else {
        
        if ((resultado < -9999999999999999 || resultado > 9999999999999999) && resultado.toString().length >= LONGITUD_MAX) {
          
            pantalla.value = resultado.toExponential(1);

        } else if (resultado.toString().length > LONGITUD_MAX) {
            
            pantalla.value = (resultado.toString().substring(16, 0));
            
        } else{
            
            pantalla.value = parseFloat(resultado.toString());
        }
       
    }
}